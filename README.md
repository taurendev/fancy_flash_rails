# Fancy_Flash_Rails

This gem is a simple to use, simple to install, simple to configure solution for a growler like flash notification in your rails 4+ application.

This gem is still in active development, should you find an issue with this gem please open a new issue in the gems repo.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fancy_flash_rails'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fancy_flash_rails

## Usage

TODO: Write usage instructions here

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/taurendev/fancy_flash_rails. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
