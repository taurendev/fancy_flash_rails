# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fancy_flash_rails/version'

Gem::Specification.new do |spec|
  spec.name          = "fancy_flash_rails"
  spec.version       = FancyFlashRails::VERSION
  spec.authors       = ["Shawn Wilson"]
  spec.email         = ["swilson@taurenltd.com"]

  spec.summary       = %q{A Simple to impliment solution for growler like flash notifications}
  spec.description   = %q{Growler like flash messages for rails 4+}
  spec.homepage      = "https://gitlab.com/taurendev/fancy_flash_rails.git"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
end
